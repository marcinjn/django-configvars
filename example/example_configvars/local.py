from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

DB_NAME = BASE_DIR / "overriden_db_name.db"
